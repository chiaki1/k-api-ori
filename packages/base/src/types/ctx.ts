export interface ServiceConfigBinding {
  id: symbol;
  constructor: {
    new (...args: any[]): any;
  };
  singleton: boolean;
}
