import { createModule, gql } from 'graphql-modules';
import { Book } from './providers/Book';
import resolvers from './resolvers';

export const TestGraphQlModule = createModule({
  id: 'blog',
  providers: [Book],
  resolvers,
  typeDefs: gql`
      type Mutation {
      }
      type Query {
          books: [Book]
      }
      type Book {
          id: String
          title: String
          author: Author
      }

      type Author {
          id: Int!
          firstName: String
          lastName: String
          books: [Book]
      }
  `
});
