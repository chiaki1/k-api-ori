import { injectable } from 'inversify';

@injectable()
export class Cfg {
  static $id = Symbol.for('Cfg');

  __config: any = {};
  setConfig(key: string, value: any) {
    this.__config[key] = value;
  }
  getConfig(key: string): any | undefined {
    return this.__config[key];
  }
}
