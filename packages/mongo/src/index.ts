import mongoose from 'mongoose';
import { logger } from '@k/base';

export const connectMongo = (uri: string) => {
  mongoose
    .connect(uri, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    })
    .then(() => {
      logger.info('Connect mongo successfully');
    })
    .catch(err => {
      logger.error(
        `MongoDB connection error. Please make sure MongoDB is running.`,
        err
      );
      throw new Error('Could not connect to mongo');
    });
};

export { Schema, Document, model } from 'mongoose';
