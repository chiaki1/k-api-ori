import defaultEnv from './default.environment';
import prodEnv from './production.environement';
import stgEnv from './staging.environment';
import { logger } from '@k/base';

export const ENV: any = defaultEnv;
logger.info(process.env.ENVIRONMENT);
logger.info(process.env.NODE_ENV);
if (process.env.ENVIRONMENT === 'production') {
  logger.info('Loaded production env');
  Object.assign(ENV, prodEnv);
} else if (process.env.ENVIRONMENT === 'staging') {
  logger.info('Loaded staging env');
  Object.assign(ENV, stgEnv);
} else if (process.env.ENVIRONMENT === 'development') {
  logger.info('Loaded development env');
  Object.assign(ENV, stgEnv);
} else {
  throw new Error('Could not load environment');
}
