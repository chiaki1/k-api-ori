import { StoreConfig } from '../models/store-config';

export default {
  Query: {
    storeConfigs: (
      _root: any,
      args: {
        userId: string;
        storeId: string;
      }
    ) => {
      return StoreConfig.find({ userId: args.userId, storeId: args.storeId });
    }
  }
};
