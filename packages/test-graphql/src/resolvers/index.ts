import query from './query';
import book from './book';

export default {
  ...query,
  ...book
};
