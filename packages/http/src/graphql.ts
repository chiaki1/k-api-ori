import { createApplication } from 'graphql-modules';
import { TestGraphQlModule } from '@k/test-graphql';
import { ConfigGraphqlModule } from '@k/config-graphql';

export const graphQLApp = createApplication({
  modules: [TestGraphQlModule, ConfigGraphqlModule]
});
