import { Document, Schema, model } from '@k/mongo';

export interface IStoreConfig extends Document {
  userId: string;
  storeId: string;
  key: string;
  value: any;
}
const StoreConfigSchema = new Schema({
  userId: { type: String, required: true },
  storeId: { type: String, required: true },
  key: { type: String, required: true },
  value: { type: Schema.Types.Mixed, required: true }
});

export const StoreConfig = model<IStoreConfig>(
  'StoreConfig',
  StoreConfigSchema
);
