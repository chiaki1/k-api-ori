import { Book } from '../providers/Book';

export default {
  Query: {
    books: (_root: any, _args: {}, { injector }: GraphQLModules.Context) =>
      injector.get(Book).allBook()
  }
};
