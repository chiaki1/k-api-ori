import { Book } from '../providers/Book';

export default {
  Book: {
    id: (book: any) => book._id,
    title: (book: any) => book.title,
    author: (book: any, _args: {}, { injector }: GraphQLModules.Context) =>
      injector.get(Book).getAuthor(book.authorId)
  }
};
