module.exports = {
  apps: [
    {
      name: 'api-node2',
      script: './packages/g-app/index.js',
      instances: 'max',
      exec_mode: 'cluster',
      autorestart: true,
      max_memory_restart: '2G',
      watch: false,
      env: {
        NODE_ENV: 'production',
        PCMS_URL: 'https://pcms.yutang.vn',
      },
      env_production: {
        NODE_ENV: 'production',
        PCMS_URL: 'https://pcms.yutang.vn',
      },
    },
  ],
};
