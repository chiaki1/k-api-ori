import { Injectable } from 'graphql-modules';

const books = [
  {
    _id: 0,
    authorId: 0,
    title: 'Title 1'
  },
  {
    _id: 1,
    authorId: 1,
    title: 'Title 2'
  },
  {
    _id: 2,
    authorId: 0,
    title: 'Title 3'
  }
];

@Injectable()
export class Book {
  constructor() {}

  getBooksOf(userId: number) {
    return books.filter(({ authorId }) => userId === authorId);
  }

  allBook() {
    return books;
  }

  getAuthor(postId: number) {
    const post = books.find(({ _id }) => _id === postId);

    if (post) {
      return {
        id: post.authorId,
        firstName: 'String',
        lastName: 'String',
        posts: [post]
      };
    }

    return null;
  }
}
