import 'reflect-metadata';
import { ENV } from './environments';
logger.info('ENV', ENV);

import express from 'express';
import compression from 'compression';
import { pingHandler } from './controllers';
import cors from 'cors';
import { pcmsProxy } from './middlewares';
import { logger } from '@k/base';
// import { graphQLApp } from './graphql';
// import { graphqlHTTP } from 'express-graphql';
// import bodyParser from 'body-parser';
import { connectMongo } from '@k/mongo';

connectMongo(ENV.MONGO_URI);

const app = express();

/* TODO: Temporary disable graphql because we not yet used it*/
// app.use(
//   '/graphql',
//   bodyParser.json(),
//   graphqlHTTP({
//     schema: graphQLApp.schema,
//     customExecuteFn: graphQLApp.createExecution(),
//     graphiql: true
//   })
// );

/* ____________________MIDDLEWARES____________________*/
app.use(compression()); // compresses requests
app.use('*', cors());
app.use('*', pcmsProxy);

/* ____________________CONTROLLERS____________________*/
app.get('/ping', pingHandler);

export default app;
