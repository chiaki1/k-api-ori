import { Request, Response } from 'express';
import { Cfg, ctx, logger } from '@k/base';

export const pingHandler = (_req: Request, res: Response) => {
  logger.debug('ping handler');
  const cfg: Cfg = ctx().get(Cfg.$id);
  cfg.setConfig('greeting', 'Hello!');
  res.json({ mess: cfg.getConfig('greeting') });
};
