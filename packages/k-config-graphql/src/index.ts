import { createModule, gql } from 'graphql-modules';
import resolvers from './resolvers';

export const ConfigGraphqlModule = createModule({
  id: 'kConfigGraphqlModule',
  resolvers,
  typeDefs: gql`
    input StoreConfigInput {
      userId: String!
      storeId: String!
      key: String!
      value: String!
    }
    type Mutation {
      setStoreConfigs(input: StoreConfigInput): StoreConfig
    }
    extend type Query {
      storeConfigs(userId: String!, storeId: String): [StoreConfig]
    }
    type StoreConfig {
      userId: String!
      storeId: String!
      key: String!
      value: String!
    }
  `
});
