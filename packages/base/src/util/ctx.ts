import { Container } from 'inversify';
import { ServiceConfigBinding } from '../types';
const container = new Container();

export const ctx = () => {
  function get<T>(symbol: symbol): T {
    return container.get(symbol);
  }

  return {
    services: (serviceConfigBindings: ServiceConfigBinding[]) => {
      serviceConfigBindings.forEach(value => {
        if (value['singleton'] === true) {
          container
            .bind(value.id)
            .to(value.constructor)
            .inSingletonScope();
        } else {
          container.bind(value.id).to(value.constructor);
        }
      });
    },
    get
  };
};

export { injectable, inject } from 'inversify';
