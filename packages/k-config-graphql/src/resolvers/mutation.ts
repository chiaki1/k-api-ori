import { StoreConfig } from '../models/store-config';

export default {
  Mutation: {
    setStoreConfigs: async (
      _root: any,
      args: {
        input: any;
      }
    ) => {
      await StoreConfig.update(
        {
          userId: args.input.userId,
          storeId: args.input.userId,
          key: args.input.key
        },
        args.input,
        {
          upsert: true,
          setDefaultsOnInsert: true
        }
      );

      return StoreConfig.findOne({
        userId: args.input.userId,
        storeId: args.input.userId,
        key: args.input.key
      });
    }
  }
};
