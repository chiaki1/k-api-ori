import { ctx } from './util';
import { Cfg } from './models';

ctx().services([
  {
    id: Cfg.$id,
    singleton: true,
    constructor: Cfg
  }
]);

export * from './models';
export * from './types';
export * from './util';
