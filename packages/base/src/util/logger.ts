import winston, { format } from 'winston';

const options: winston.LoggerOptions = {
  level: 'silly',
  levels: winston.config.cli.levels,
  transports: [
    new winston.transports.Console({
      format: format.combine(
        format.splat(),
        format.cli(),
        format.metadata(),
        format.simple(),
        format.errors()
      )
    })
  ]
};

export const logger = winston.createLogger(options);
